//定义reducer的初始值
const init = {
  movieList: []
}
export default function countReducer(prevStata = init, action) {
  const {
    type,
    data
  } = action;

  switch (type) {
    case 'getmovielist':
      return {
        ...prevStata,
        movieList: data
      }
  }

  return prevStata;
}