import React, { useState, useEffect } from 'react'
// 引入Pubsub的插件
import PubSub from 'pubsub-js'
export default function Two() {
  //刚开始的数据无法获取 
  const [car, setCar] = useState('八手奥拓')
  // 在组件初始化的时候订阅一次 名字要和发布的名字一致才行
  // 修改自己状态里的值
  useEffect(() => {
    PubSub.subscribe("vehicle", (_, data) => {
      setCar(data)
    })
  }, []);
  return (
    <div>
      <h2>我弟弟 我开的 {car}</h2>
    </div>
  )
}
