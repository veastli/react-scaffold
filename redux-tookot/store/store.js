import {
  configureStore
} from '@reduxjs/toolkit';

import countReducer from './slice/countSlice'
import movieReducer from './slice/moviesSlice'
const store = configureStore({
  reducer: {
    count: countReducer,
    movie: movieReducer
  }
})

export default store