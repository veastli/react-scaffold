import React, { useState, useContext } from 'react'
// 引入css文件并使用
import './App.css'
import { nanoid } from 'nanoid'
import Header from './components/Header'
import Footer from './components/Footer'
import List from './components/List'
// 状态提升就是相邻的兄弟都想使用同一个组件的话 就给他们的父组件置state状态保存数据
// 共享对象AppContext上有一个Provide属性是一个组件,他可以给组件内部包含的组件提供数据,提供的数据放在value属性上
//创建一个可以提供数据的 上下文对象
export const AppContext = React.createContext();
export default function App() {
  const [todoList, setTodoList] = useState([
    { id: nanoid(), todo: "唱", done: false },
    { id: nanoid(), todo: "跳", done: false },
    { id: nanoid(), todo: "rap", done: false },
  ])
  return (
    <div className="todo-container">
      <div className="todo-wrap">
        <AppContext.Provider value={[todoList, setTodoList]}>
          <Header />
          <List />
          <Footer />
        </AppContext.Provider>
      </div>
    </div>
  )
}
