import React, {useEffect } from 'react'
import axios from 'axios'

//可以接受一个初始值
export default function App() {
  // 刚开始加载的时候发送请求
  useEffect(() => {
    // 发送axios请求
    axios.get('/api1/user')
  }, [])
  return <div>
    App
  </div>
}