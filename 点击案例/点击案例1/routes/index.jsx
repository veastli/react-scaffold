// 引入路由
import { Navigate, useRoutes } from 'react-router-dom'
import Login from '../components/Login'
import Main from '../components/Main'
import NotFound from '../components/404'
import Home from '../components/Main/Home'
import About from '../components/Main/About'
const routes = [
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/main',
    element: <Main />,
    children: [
      {
        path: '/main/about',
        element: <About />,
      },
      {
        path: '/main/home',
        element: <Home />,
      },

    ]
  },
  {
    path: '/',
    element: <Navigate to="/login" />
  },
  {
    path: '*',
    element: <NotFound />
  },
]
// 这里会把route使用 useRoutes直接展开用于根据路由表,动态创建Routes和Route 直接暴露出去 外部直接引入
const GetRoutes = () => {
  return useRoutes(routes)
}
export default GetRoutes