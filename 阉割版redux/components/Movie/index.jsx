import React from 'react'
import store from "../../store/store"
export default function Movie() {
  const { count } = store.getState()
  return (
    <div>
      <h1>我是Movie组件</h1>
      <p>我收到的Count组件的值是 {count}</p>
    </div>
  )
}
