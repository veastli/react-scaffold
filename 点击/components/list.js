import React from 'react'
import { useLocation, useParams } from 'react-router-dom'
export default function List() {
  // 接受state传参
  const location = useLocation()
  const state = location.state
  return (
    <div>
      <p>用户名是{},编号是:{}</p>
      <ul>
        {state.map(item => <li key={item.id}>{item.name}</li>)}
      </ul></div>
  )
}
