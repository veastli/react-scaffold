import {
  configureStore
} from '@reduxjs/toolkit';
// 引入reducer
import countReducer from './slice/countSlice'
import movieReducer from './slice/movieSlice'
const store = configureStore({
  reducer: {
    count: countReducer,
    movie: movieReducer
  }
})
export default store