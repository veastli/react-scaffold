import { nanoid } from "nanoid";
import React, { useEffect, useState } from "react";
import './App.css'
// 引入nanoid的插件(随机的id值)
import Footer from "./components/Footer";
import Header from './components/Header'
import List from './components/List'
export default function App() {
  // 状态提升 把数据放在App里
  // 初始化todoList数据 需要引入useState来实现
  const [todoList, setTodoList] = useState([
    { id: nanoid(), todo: "唱", done: false },
    { id: nanoid(), todo: "跳", done: false },
    { id: nanoid(), todo: "Rap", done: false }
  ])
  // 刚开始渲染App的时候 去本地存储中查看是否保存有数据 []代表刚开始渲染的时候
  useEffect(() => {
    // JSON.parse(json) json对象(数组)(字符串类型)-->转换为js对象(数组)
    // Storage.getItem('key');
    // 该方法接受一个键名作为参数，返回键名对应的值。
    setTodoList(JSON.parse(localStorage.getItem("todoList")) || [])
  }, [])
  //监听todoList 如果发生改变 则在本地保存 Storage.setItem('key', 'value');
  // 该方法接受一个键名和值作为参数，将会把键值对添加到存储中，如果键名存在，则更新其对应的值。
  useEffect(() => {
    localStorage.setItem("todoList", JSON.stringify(todoList))
  }, [todoList])
  return (<div className="todo-container">
    <div className="todo-wrap">
      {/* 状态提升 给子代传递值和修改值的方法 */}
      <Header todoList={todoList} setTodoList={setTodoList} />
      {/* 状态提升 给子代传递值和修改值的方法 */}
      <List todoList={todoList} setTodoList={setTodoList} />
      <Footer todoList={todoList} setTodoList={setTodoList} />
    </div>
  </div>)
}