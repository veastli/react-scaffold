import {
  GETMOVIELIST
} from "../constant/movieConst"


//定义reducer的初始值
const init = {
  movieList: []
}


export default function countReducer(prevStata = init, action) {
  const {
    type,
    data
  } = action;
  switch (type) {
    case GETMOVIELIST:
      return {
        ...prevStata,
        movieList: data
      }
    default:break
  }

  return prevStata;
}