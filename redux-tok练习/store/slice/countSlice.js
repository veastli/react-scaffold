//引入
import {
  createSlice,
  createAsyncThunk
} from "@reduxjs/toolkit";

//我们使用createAsyncThunk方法来创建异步action
//createAsyncThunk接受两个参数,第一个参数是type的值,第二个参数是回调函数,回调函数就是异步action的函数
import { movieRequest } from '../../api'
// createSlice：接受一个初始状态和一个带有reducer名称和函数的查找表，并自动生成action creator函数、action类型字符串和一个reducer函数
// 1.调用createSlice方法
const countReducer = createSlice({
  // action中type的命名空间 type的前缀
  name: 'counter',
  // reducer提供的初始值
  initialState: {
    count: 0
  },
  // 配置reducers 将来会帮你自动生成
  reducers: {
    // 加 prev初始值 
    add(prev, action) {
      //reduce中处理函数接收两个参数 分别是当前reducer中上一次的值\本次dispacth的action对象
      prev.count += action.payload
    },
    // 减
    sub(prev, action) {
      prev.count -= action.payload
    }
  }
})
// 创建异步的action请求 并导出
export const getMovieAction = createAsyncThunk('movie/getMovieList', async () => {
  // 在当前函数中执行异步操作
  const result = await movieRequest()
  return result.data.data.list
})
const movieSlice = createSlice({
  // 命名name值为action type的前缀
  name: 'movie',
  initialState: {
    movieList: []
  },
  reducers: {
  },
  //异步action的处理,需要书写额外的reducer处理,不能写在reducers中,因为reducers会创建一个对应的action
  extraReducers: (builder) => {
    builder.addCase(getMovieAction.fulfilled, (state, action) => {
      state.movieList = action.payload
    })
  }
})
export const reduceTwo = movieSlice.reducer
// 通过createSlice的reducers属性 直接得到就是当前的reducer函数,可以暴漏出去供store使用
export const reducerOne = countReducer.reducer
// 暴漏action
export const {
  add,
  sub
} = countReducer.actions;

