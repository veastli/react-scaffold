import React from 'react'
import store from "../store/store"
import { getMovieAction } from '../store/actions/movieAction'
export default function Movie() {
  // 获取库里的值
  const { count } = store.getState()
  const { movieList } = store.getState().movie;
  const getMovieHandle = () => {
    store.dispatch(getMovieAction())
  }

  return (
    <div>
      <h1>我是Movie组件</h1>
      <p>我收到的Count组件的值是 {count}</p>
      <button onClick={getMovieHandle}>获取电影的数据</button>
      <ul>
        {
          movieList.map(item => {
            return <li key={item.tvId}>{item.albumName}</li>
          })
        }
      </ul>
    </div>

  )
}
