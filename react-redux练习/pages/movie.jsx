import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getMovieAction } from '../store/slice/movieSlice'

export default function Movie() {
  const dispatch = useDispatch()
  const count = useSelector(state => state.count.count)
  // 这里的movie就是store中的那一层
  const movieList = useSelector(state => state.movieList.movieList)
  // console.log(movieList,'movieList');
  // 获取信息的函数
  const getMovieHandle = () => {
    dispatch(getMovieAction())
  }
  return (
    <div>
      <h1>我是Movie组件</h1>
      <p>我收到的Count组件的值是 {count}</p>
      <button onClick={getMovieHandle}>获取电影的数据</button>
      <ul>
        {
          movieList.map(item =><li key={item.tvId}>{item.albumName}</li>
          )
        }
      </ul>
    </div>
  )
}
