// 引入applyMiddleware帮我调用这个函数
import { createStore, applyMiddleware, combineReducers } from "redux";
// 引入reducers文件
import countReducer from './reducers/countReducer'
import movieReducer from "./reducers/movieReducer"
// 创建store 并提供对应的reducer
import thunk from 'redux-thunk';
//异步处理中间件
//使用redux提供的combineReducers方法 合并多个reducer
const allReducer = combineReducers({
  movie: movieReducer,
  count: countReducer
})

//当我们要进行异步action，action是一个函数，我们dispacth的函数就会提交到中间件
//中间件帮我们执行函数，一般函数中会有一个dispatch再次派发到store中
const store = createStore(allReducer, applyMiddleware(thunk));
export default store;