import React from 'react'
// 接受父组件传来的值
export default function Item({ avatar_url, login, url }) {
  return (
    <div className="card">
      <a href={url} target='_blank'>
        <img src={avatar_url} style={{ width: "100px" }} />
      </a>
      <p className="card-text">{login}</p>
    </div>
  )
}
