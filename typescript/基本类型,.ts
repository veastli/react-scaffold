// 基础类型
// 布尔值(Boolean) 数字(number) 字符串(string) undefined null Symbol  bigint
// 最基本的数据类型就是简单的true/false值,在js和ts里叫做boolean
// ts类型:boolean类型
let boo1 = true
let sss: boolean = true
//声明变量的时候使用类型声明
//
{
  const arr: number[] = [1, 2, 3, 4]
  arr.push(2)
  // | 或 联合类型
  const arr1: (number | string)[] = [1, 2, 3, 4]
  arr1.push('联合类型')
  // 使用泛型定义数组类型

}
// 元组Tuple 已知已知类型,长度固定的数组
// 元祖可以被方法越界(越界的值必须符合之前的类型限定 )
let t1: [string, number] = ['hello', 1]
// enum 枚举 为一组数值
enum Color {
  red,
  green,
  blue,
  distance
}
// any 解决临时报错 可以在编码阶段给未知的数据添加类型,但是未来还是要在类型上补上
// void类型 代表没有任何类型 最常见的就是函数没有返回值某种程度上来说，void 类型像是与 any 类型相反，它表示没有任何类型。 当一个函数没有返回值时，你通常会见到其返回值类型是 void：
// 写z
function fn() {
  return fn
}
// object类型时除了原始类型以外所有的类型
function fn1(): object {
  return fn()
}
// 联合类型 可以被多个选择
let c1: number | string = 3
c1 = 'abc'
// c1 = true // error
// 类型断言  as 告诉是什么类型
function getLength(x: number | string) {
  if ((<string>x).length) {
    return (x as string).length
  } else {
    return x.toString().length
  }
}

console.log(getLength('abcd'), getLength(1234))
// 类型别名 可以给联合类型起别名
// 假设对象种有一个属性值时是一个数组 我们可以先把数组的类型定义出来,将来在对象类中
// 接口用来定义一个对象的类型 interface
//'?' 可选属性
// 只读属性
// 函数类型
interface obj1Type {
  name: string,
  readonly age: number//readonly只读
}
const obj1: obj1Type = {
  name: '罗飞',
  age: 20
}

