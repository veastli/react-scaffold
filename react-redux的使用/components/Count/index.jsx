import React from 'react'
// 引入react提供的hook
import { useSelector, useDispatch } from 'react-redux'
import { incrementAction, decrementAction, waitIncrementAction } from '../../store/actions/countAction';

export default function Count() {
  // 返回Redux的store中对dispatch的引用
  const dispatch = useDispatch()
  const count = useSelector(state => state.count.count)
  const movieList = useSelector(state => state.movie.movieList)
  const incrementHandle = () => {
    dispatch(incrementAction(1))
  }

  const decreamentHandle = () => {
    dispatch(decrementAction(1))
  }

  const ifOddDecrementHandle = () => {
    if (count % 2 !== 0) {
      dispatch(incrementAction(1))
    }
  }

  const waitDecrementHandle = () => {
    dispatch(waitIncrementAction(1))
  }



  return (
    <div>
      <h1>我是Count组件 我的值是{count}</h1>
      <h2>当前的电影总是数 {movieList.length}</h2>
      <div>
        <button onClick={incrementHandle}>累加</button>
        <button onClick={decreamentHandle}>累减</button>
        <button onClick={ifOddDecrementHandle}>如果是奇数再加1</button>
        <button onClick={waitDecrementHandle}>等2s再加1</button>
      </div>

    </div>
  )
}
