import React from 'react'
// outlet来渲染子路由
import { Outlet, NavLink } from "react-router-dom";
export default function Main() {
  return (
    <div>
      <div className="row">
        <div className="col-xs-offset-2 col-xs-8">
          <div className="page-header"><h2>React Router Demo</h2></div>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-2 col-xs-offset-2">
          <div className="list-group">
            <NavLink className="list-group-item" to="/main/about">About</NavLink>
            <NavLink className="list-group-item" to="/main/home">Home</NavLink>
          </div>
        </div>
        <div className="col-xs-6">
          <div className="panel">
            <div className="panel-body">
              <Outlet />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
