// 这里面是count的请求把请求的action对象提出来放在这
import store from '../store'
export const incrementAction = (data = 1) => {
  return {
    type: 'incrementCount',
    data
  }
}
//累减的action
export const decrementAction = (data = 1) => {
  return {
    type: 'decrementCount',
    data
  }
}
// 异步action 
export const waitIncrementAction = (data) => {
  return () => {
    setTimeout(() => {
      store.dispatch({
        type: 'incrementCount',
        data
      })
    }, 2000)
  }
}