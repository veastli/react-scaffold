// 所有组件的外壳文件
import React from 'react'
import Movie from './pages/movie.jsx'
import Count from './pages/count.jsx'
export default function App() {
  return (
    <div>
      <Movie />
      <hr />
      <Count />
    </div>
  )
}


