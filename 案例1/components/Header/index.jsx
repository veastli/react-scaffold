import React from 'react'
import "./index.css"
import { nanoid } from 'nanoid'
// 接受父代传递来的值 和用来修改值的函数
export default function Header({ todoList, setTodoList }) {
  // 发布事件函数 按键抬起事件
  const KeyRelease = (e) => {
    // 判断是否按下的是回车 如果不是 则直接不执行下面的
    if (e.keyCode !== 13) return
    // 再获取输入的内容 并判断是否为空 通过e.target来获取元素 value是元素的值 trim()是清除空格
    const InputContent = e.target.value.trim()
    if (!InputContent) return alert('请输入正确的内容')
    // 以上都不满足则执行下面的行为
    // 创建新的todo对象
    const newTodo = { id: nanoid(), todo: InputContent, done: false }
    // 调用父组件的方法来改变
    setTodoList([...todoList, newTodo])
    // 清空输入框
    e.target.value = ''
  }
  return (
    <div className="todo-header">
      {/* 添加一个按键抬起事件 */}
      <input type="text" onKeyUp={KeyRelease} placeholder="请输入你的任务名称，按回车键确认" />
    </div>
  )
}
