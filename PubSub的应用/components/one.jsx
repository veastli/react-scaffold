import React, { useState } from 'react'
// 引入Pubsub的插件
import PubSub from 'pubsub-js'
export default function One() {
  const [car, setCar] = useState('布加迪-雅迪')
  const changeCarHandle = () => {
    setCar('劳斯莱斯')
    // 更新数据时发布数据
    PubSub.publish('vehicle', '劳斯莱斯')
  }
  return (
    <div>
      <h1>One</h1>
      <p>我的车是{car}</p>
      <button onClick={changeCarHandle}>换车</button>
    </div>
  )
}
