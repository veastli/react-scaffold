// 入口文件
import React from "react";
import ReactDOM from 'react-dom/client';
import App from "./App";
// 引入store
import store from "./store/store";
// 引入react=redux提供的库
import { Provider } from "react-redux";
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
        <App />
    </Provider>)