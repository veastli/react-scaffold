// 设置初始值
const init = { count: 0 }
// 函数接收两个参数 第一个参数是原来的状态 默认是没有的 所以需要我们手动添加一个 第二个是接受action传过来的值
export default function countReducer(prevStata = init, action) {
  // 通过解构赋值拿到传过来的
  const { type, data } = action
  // 通过type传过来的类型 进行判断来进行后续的执行
  switch (type) {
    // 如果是incrementCount 则进行加法操作
    case 'incrementCount':
      return {
        ...prevStata, count: prevStata.count + data
      }
      // 如果是decrementCount 则进行减法操作
    case 'decrementCount':
      return {
        ...prevStata, count: prevStata.count - data
      };
  }

  // 返回初始值
  return prevStata
}