// 创建store链接reducers   
import {
  configureStore
} from '@reduxjs/toolkit';

import { reducerOne, reduceTwo } from './slice/countSlice'


//合并多个reducer,并给每一个reducer返回的数据用一个key保存
const store = configureStore({
  reducer: {
    count: reducerOne,
    movie: reduceTwo
  }
})

export default store