import React, { useState } from 'react'
import "./index.css"
// 这里要接受父代传递的值 传递的是一个对象 把对象结构出来{}
export default function Item({ todo, done, id, setTodoList, todoList }) {
  //isActive状态用来保存li的active类是否添加和按钮的display状态
  const [isActive, setIsActive] = useState(false)
  // 给li添加一个鼠标移入移出事件 传入一个值
  const mouseHandle = (bool) => {
    //外层函数用来接收参数,返回的函数是真正的事件函数
    return () => {
      //  修改这个值
      setIsActive(bool)
    }
  }
  // 多选框触发改变的事件函数
  const checkChangeHandle = (e) => {
    // 遍历并修改对应的done的状态 
    const newTodo = todoList.map(item => {
      // 找到对应的那个状态里的数值 进行修改
      if (item.id === id) {
        // 这个列表的状态被修改 重新渲染
        // item.done = e.target.checked
        return { ...item, done: e.target.checked }
      }
      // item返回出去
      return item
    })
    setTodoList(newTodo)
  }

  const Delete = () => {
    const OldTodo = todoList.filter(item => item.id !== id)
    setTodoList(OldTodo)
  }

  return (
    <li onMouseEnter={mouseHandle(true)} onMouseLeave={mouseHandle(false)} className={isActive ? 'active' : ''}>
      <label>
        <input type="checkbox" checked={done} onChange={checkChangeHandle} />
        <span>{todo}</span>
      </label>
      <button className="btn btn-danger" style={{ display: isActive ? 'block' : 'none' }} onClick={Delete}>删除</button>
    </li>
  )
}
