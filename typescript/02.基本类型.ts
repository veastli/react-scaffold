//类型注释 在变量后面加类型
const a: number = 10

const b: boolean = true
// 
// interface  用来部署接口
interface Person {
  name: string
  age: number
}
// 在变量后面放置一个接口就可以控制里面的内容
const arr: Person = {
  name: 'luof',
  age: 12
}
// 元组 Tuple
// 元组类型允许表示一个已知元素数量和类型的数组，各元素的类型不必相同。 比如，你可以定义一对值分别为 string 和 number 类型的元组。
let t1: [string, number] = ['hello', 200]
// 枚举
// enum类型js标准数据类型的一盒补充.使用枚举类型可以为一组数据赋值予友好的名字
enum Name {
  mark,
  jack,
  lorry
}
console.log(Name[0], Name.mark);
// any 标记所有想要标记的对象 它可以是任何值
// void某种程度上来说，void 类型像是与 any 类型相反，它表示没有任何类型。 当一个函数没有返回值时，你通常会见到其返回值类型是 void：
interface Person1 {
  name: any
  age: object
}
function fn() {
  console.log(1);

}
// object 表示非原始类型，也就是除 number，string，boolean之外的类型。
// 使用 object 类型，就可以更好的表示像 Object.create 这样的 API。例如：
// 联合类型 表示取值可以为多种类型中一种
let data: number | string = 'a'
// 类型断言
// 通过类型断言这种方式可以告诉编译器，“相信我，我知道自己在干什么”。 类型断言好比其它语言里的类型转换，但是不进行特殊的数据检查和解构。 它没有运行时的影响，只是在编译阶段起作用。 TypeScript 会假设你，程序员，已经进行了必须的检查。
// xx as 类型
// <类型/> xx

