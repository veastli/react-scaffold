import React from 'react'
// 引入样式
import './App.css'
import Header from './components/Header/App.jsx'
import List from './components/List/App.jsx'
export default function App() {
  return (
    <div className="container">
      <Header />
      <List />
    </div>
  )
}
