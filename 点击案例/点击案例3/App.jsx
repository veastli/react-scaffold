// 所有组件的外壳文件
import React from 'react'
// 引入NavLink
import { NavLink, useRoutes } from 'react-router-dom'
// 引入路由表 下载组件useRoutes解析路由表
import routes from './routes'
// 生成函数式组件
export default function App() {
  const element = useRoutes(routes)
  return (
    <div id="root">
      <div>
        <div className="row">
          <div className="col-xs-offset-2 col-xs-8">
            <div className="page-header"><h2>React Router Demo</h2></div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-2 col-xs-offset-2">
            <div className="list-group">
              {/* 路由链接 */}
              <NavLink className="list-group-item " to="/about">About</NavLink>
              <NavLink className="list-group-item" to="/home">Home</NavLink>
            </div>
          </div>
          <div className="col-xs-6">
            <div className="panel">
              <div className="panel-body">
                {/* 一般路由都是部署在外面的 统一部署在路由表上 */}
                {/* 注册路由 */}
                {/* 点击哪个路由链接 就会跳转指定的路由组件 */}
                {/* <Routes>
                  <Route path='/about' element={<About />} />
                  <Route path='/home' element={<Home />} />
                  <Route path='/' element={<Navigate to='about' />} />
                </Routes> */}
                {/* 插入解析路由表后的路由 */}
                {element}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
