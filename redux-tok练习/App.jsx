// 包裹所有组件的外壳文件 
import React from "react";
// 引入两个需要渲染的组件
import Count from './pages/Count'
import Movie from './pages/Movie'
export default function App() {
  return <div>
    <Count />
    <hr />
    <Movie />
  </div>
}