module.exports = {
  devServer: {
    // 激活代理服务器
    proxy: {
      // 将来以/dev-api开头的请求，就会被开发服务器转发到目标服务器去。
      "/api1": {
        // 需要转发的请求前缀
        target: "http://127.0.0.1:8888", // 目标服务器地址
        changeOrigin: true, // 允许跨域
        pathRewrite: {
          "^/api1": "",
        }
      }
    }
  }
}
