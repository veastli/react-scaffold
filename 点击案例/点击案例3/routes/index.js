// 引入作用重定向的Navigate组件被解析 就会修改路径,切换视图
import { Navigate } from 'react-router-dom'
// 引入react里的懒加载组件
import { lazy, Suspense } from 'react'
import Loading from '../components/404'
// 引入需要集合的路由 合成一个路由表
const About = lazy(() => import('../pages/about'))
const Home = lazy(() => import('../pages/home'))
const List = lazy(() => import('../components/list'))
const LazyLoad = (Comp) => {
  return (
    <Suspense fallback={<Loading />}>
      <Comp />
    </Suspense >
  )
}
export default [
  {
    path: '/about',
    element: LazyLoad(About),
    // 配置子路由
    children: [
      {
        path: 'list/:id',
        element: LazyLoad(List)
      }
    ]
  },
  {
    path: '/home',
    element: LazyLoad(Home),
    // 配置子路由
    children: [
      {
        path: 'list/:id',
        element: LazyLoad(List)
      }
    ]
  },
  {
    path: '/',
    element: <Navigate to='/about' />
  }
]