import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getMovieAction } from '../store/slice/countSlice'
export default function Movie() {
  const dispatch = useDispatch()
  const movieList = useSelector(state => state.movie.movieList)
  const count = useSelector(state => state.count.count)
  const GetMovieInformation = () => {
    dispatch(getMovieAction())
  }
  return (
    <div>
      <h1>Movie</h1>
      <p>Count:{count}</p>
      <button onClick={GetMovieInformation}>获取电影的数据</button>
      <ul>
        {movieList.map(item => <li key={item.tvId}>{item.albumName}</li>)}
      </ul>
    </div>
  )
}
