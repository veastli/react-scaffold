import React, { useEffect } from 'react'
// 引入样式
import './App.css'
import axios from 'axios'
// 引入组件
import Header from './components/Header'
import List from './components/List'
export default function App() {
  useEffect(() => {
    axios.get('/api1/user')
  }, [])
  return (
    <div>
      <Header />
      <List />
    </div>
  )
}
