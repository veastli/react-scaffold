import {
  createSlice,
  createAsyncThunk
} from "@reduxjs/toolkit";
//   createAsyncThun 创建异步请求
// 获取异步请求
import { movieRequest } from '../../api'
// 创建异步的action请求
export const getMovieAction = createAsyncThunk('movie/getMovieList',
  async () => {
    const res = await movieRequest()
    console.log(res, 'res');
    return res.data.data.list
  }
)
const movieSlice = createSlice({
  name: "movie",
  // 初始值
  initialState: {
    movieList: []
  },
  reducers: {
  },
  // 异步action的处理
  extraReducers: (builder) => {
    builder.addCase(getMovieAction.fulfilled, (state, action) => {
      state.movieList = action.payload
    })
  }
})
export default movieSlice.reducer