import {
  movieRequest
} from '../../api'

import store from '../store';

export const getMovieAction = () => {
  return async () => {
    const re = await movieRequest();
    store.dispatch({
      type: 'getmovielist',
      data: re.data.data.list
    })
  }
}