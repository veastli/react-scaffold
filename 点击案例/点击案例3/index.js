// 入口文件
// 引入react插件
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
//把整个App中的组件放在一个路由器中,我们使用的是</BrowserRouter>
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);