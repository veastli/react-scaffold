import React from 'react'
import store from '../store/store'
// 引入action的组件 来进行使用
import { incrementAction, decrementAction, waitIncrementAction } from '../store/actions/countActions';
export default function Count() {
  // 获取到reducer返回的值 getState()调用返回的值
  const { count } = store.getState()
  const { movieList } = store.getState().movie
  // 累加
  const incrementHandle = () => {
    // 修改传过来的值 使用action并传参
    store.dispatch(incrementAction(1))
  }
  // 累减
  const decreamentHandle = () => {
    store.dispatch(decrementAction(1))
  }
  // 如果是奇数再加1
  const ifOddDecrementHandle = () => {
    if (count % 2 == 1) {
      store.dispatch(incrementAction(1))
    }
  }
  // 等2秒再加一
  const waitDecrementHandle = () => {
    store.waitIncrementAction(1)
  }
  return (
    <div>
      <h1>我是Count组件 我的值是{count}</h1>
      <h2>当前的电影总是数 {movieList.length}</h2>
      <div>
        <button onClick={incrementHandle}>累加</button>
        <button onClick={decreamentHandle}>累减</button>
        <button onClick={ifOddDecrementHandle}>如果是奇数再加1</button>
        <button onClick={waitDecrementHandle}>等2s再加1</button>
      </div></div>
  )
}
