import {
  INCREMENTCOUNT,
  DECREMENTCOUNT
} from '../constant/countConst'
//定义reducer的初始值
const init = {
  count: 0
}

export default function countReducer(prevStata = init, action) {
  // console.log("reducer调用了", prevStata, action);

  //接受action对象的type和data
  const {
    type,
    data
  } = action;

  switch (type) {
    case INCREMENTCOUNT:
      return {
        ...init, count: prevStata.count + data
      };

    case DECREMENTCOUNT:
      return {
        ...init, count: prevStata.count - data
      }
    default: break
  }

  return prevStata;
}