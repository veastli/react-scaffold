import {
  INCREMENTCOUNT,
  DECREMENTCOUNT
} from '../constant/countConst'
import store from '../store'
//累加的action
export const incrementAction = (data = 1) => {
  return {
    type: INCREMENTCOUNT,
    data
  }
}

//累减的action
export const decrementAction = (data = 1) => {
  return {
    type: DECREMENTCOUNT,
    data
  }
}

//异步action
export const waitIncrementAction = (data) => {
  return () => {
    setTimeout(() => {
      store.dispatch({
        type: INCREMENTCOUNT,
        data
      })
    }, 2000)
  }
}