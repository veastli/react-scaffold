import { GETMOVIELIST } from '../constant/countConst'
// 定义reducer的初始值
const init = {
  movieList: []
}
export default function countReducer(prevState = init, action) {
  const {
    type,
    data
  } = action;
  switch (type) {
    case GETMOVIELIST:
      return {
        ...prevState,
        movieList: data
      }
  }
  return prevState
}