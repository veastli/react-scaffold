// 配置入口文件
// 引入react 
import React from "react";
// 引入ReactDOM 渲染页面
import ReactDOM from 'react-dom/client';
import App from './App'
// 引入路由器BrowserRouter来包住App
import { BrowserRouter } from 'react-router-dom'
// 引入模块
ReactDOM.createRoot(document.getElementById('root')).render(<BrowserRouter>
  <App />
</BrowserRouter>)