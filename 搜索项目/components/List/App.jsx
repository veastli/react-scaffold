import React, { useEffect, useState } from 'react'
import Item from './Item/App.jsx'
import PubSub from 'pubsub-js'

export default function List() {
  // 初始化一个state保存订阅回来的数据,当订阅回来的 数据改变再重新渲染
  const [searchList, setSearchList] = useState({ isInit: true, isSearch: false, data: [] })
  // 初始化订阅
  useEffect(() => {
    PubSub.subscribe('github', (_, data) => {
      setSearchList(data)

    })
  }, [])
  return (
    <div className="row">
      {searchList.isInit ? <h2>请开始搜索</h2 > :
        searchList.isSearch ? <h2>正在搜索中....</h2> :
          searchList.data.map(item => {
            return <Item key={item.id} {...item} />
          })
      }
    </div>
  )
}
