import { createSlice } from '@reduxjs/toolkit'
const countSlice = createSlice({
  name: 'counter',
  // 初始值
  initialState: {
    count: 0
  },
  reducers: {
    increment(prev, action) {
      prev.count += action.payload
    },
    decrement(prev, action) {
      prev.count -= action.payload
    },
  }
})

export const {
  increment,
  decrement
} = countSlice.actions;

export default countSlice.reducer