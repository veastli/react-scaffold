import React from 'react'
import "./index.css"

export default function Footer({ todoList, setTodoList }) {
  const getData = () => todoList.reduce((p, c) => c.done ? p + 1 : p, 0)
  // 点击的时候全部选中
  const CheckAll = (e) => {
    const newTodoList = todoList.map((item) => {
      return { ...item, done: e.target.checked }
    })
    setTodoList(newTodoList)
  }
  // 清除所有
  const DeleteAll = () => setTodoList(todoList.filter(item => !item.done))
  return (
    <div className="todo-footer">
      <label>
        <input type="checkbox" checked={getData() === todoList.length && todoList.length != 0} onChange={CheckAll} />
      </label>
      <span>
        <span>已完成{getData()}</span> / 全部{todoList.length}
      </span>
      {/* 给清除的按钮添加一个点击事件 封装一个函数 onClick={eliminate} */}
      <button className="btn btn-danger" onClick={DeleteAll}>清除已完成任务</button>
    </div>
  )
}
