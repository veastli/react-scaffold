import React from 'react'
// 引入重定向
import { Outlet, useNavigate } from 'react-router-dom'
export default function Home() {
  // 封装方法
  const Navigate = useNavigate()
  const toMusicHandle = () => {
    // '/'后面是param传参 类似路径 路由表中路由路径中需要加:id 才能被获取   ?后面是search传参 类似查询字符串 第二个参数写的是state传参
    Navigate('news/001?user=李沛华', {
      state: [
        { id: "001", name: "最伟大的作品" },
        { id: "002", name: "夜曲" },
        { id: "003", name: "爷爷泡的茶" },
        { id: "004", name: "求佛" },
        { id: "005", name: "白狐" }
      ]
    })
  }
  const toNewsHandle = () => {
    Navigate('news/002?user=李沛华', {
      state: [
        { id: "001", name: "31省份昨日新增本土69+278" },
        { id: "002", name: "多地提高医保最低缴费年限" },
        { id: "003", name: "岸田文雄宣称要“继承安倍遗志" }
      ]
    })
  }
  return (<div>
    <h2>Home组件内容</h2>
    <div>
      <ul className="nav nav-tabs">
        <li>
          <button onClick={toMusicHandle}>Message</button>
        </li>
        <li>
          <button onClick={toNewsHandle}>News</button>
        </li>
      </ul>
      {/* 触发了哪一个 就指向哪一个 指定路由组件呈现的位置 */}
      { }
      <Outlet />
    </div>
  </div>
  )
}
