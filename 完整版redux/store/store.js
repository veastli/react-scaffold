// 引入
import { createStore } from "redux";
// 引入reducers文件
import countReducer from './reducers/countReducer'
// 创建store 并提供对应的reducer
const store = createStore(countReducer);

export default store;