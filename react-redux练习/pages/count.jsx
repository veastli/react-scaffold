import React from 'react'
import { add, sub } from '../store/slice/countSlice'
import { useDispatch, useSelector } from 'react-redux'

export default function Count() {
  // 返回dispatch
  const dispatch = useDispatch()
  // 通过调用这个 获得值
  const count = useSelector(state => state.count.count)
  const incrementHandle = () => {
    dispatch(add(1))
  }
  const decreamentHandle = () => {
    dispatch(sub(1))
  }
  const ifOddDecrementHandle = () => {
    if (count % 2 == 1) {
      dispatch(add(1))
    }
  }
  const waitDecrementHandle = () => {
  }
  return (
    <div>
      <h1>我是Count组件 我的值是{count}</h1>
      <h2>当前的电影总是数 { }</h2>
      <div>
        <button onClick={incrementHandle}>累加</button>
        <button onClick={decreamentHandle}>累减</button>
        <button onClick={ifOddDecrementHandle}>如果是奇数再加1</button>
        <button onClick={waitDecrementHandle}>等2s再加1</button>
      </div>

    </div>
  )
}
