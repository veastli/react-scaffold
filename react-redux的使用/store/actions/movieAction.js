import {
  movieRequest
} from '../../api'

import {
  GETMOVIELIST
} from "../constant/movieConst"
import store from '../store';

export const getMovieAction = () => {
  return async () => {
    const re = await movieRequest();
    store.dispatch({
      type: GETMOVIELIST,
      data: re.data.data.list
    })
  }
}