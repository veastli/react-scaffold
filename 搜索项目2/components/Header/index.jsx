import React, { useState } from 'react'
import axios from 'axios';
import PubSub from 'pubsub-js';

export default function Header() {
  // 创建一个状态 
  const [search, setSearch] = useState('')
  const changeSearchHandle = (e) => {
    // e.target.value可以获得被选中的那个框里的值
    setSearch(e.target.value)
  }

  const searchHandle = async () => {
    // 发布事件
    PubSub.publish('github', { isInit: false, isSearch: true, data: [] })
    // 拿一个值保存请求后的返回值
    const result = await axios.get("https://api.github.com/search/users?q=" + search)
    // 请求后发送不同的值
    PubSub.publish('github', { isInit: false, isSearch: false, data: result.data.items })
  }
  return (
    <section className="jumbotron">
      <h3 className="jumbotron-heading">Search Github Users</h3>
      <div>
        {/* 值被改变把里面的值修改成state里 */}
        <input type="text" value={search} onChange={changeSearchHandle} placeholder="enter the name you search" />&nbsp;
        {/* 点击时触发事件 */}
        <button onClick={searchHandle}>Search</button>
      </div>
    </section>
  )
}
