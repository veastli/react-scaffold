import React from 'react'
import { Outlet, useNavigate } from 'react-router-dom'
export default function About() {
  // 封装方法 
  const Navigate = useNavigate()
  const toMusicHandle = () => {
    Navigate('list', {
      state: [
        { id: "001", name: "最伟大的作品" },
        { id: "002", name: "夜曲" },
        { id: "003", name: "爷爷泡的茶" },
        { id: "004", name: "求佛" },
        { id: "005", name: "白狐" }
      ]
    })
  }
  const toNewsHandle = () => {
    Navigate('list', {
      state: [
        { id: "001", name: "31省份昨日新增本土69+278" },
        { id: "002", name: "多地提高医保最低缴费年限" },
        { id: "003", name: "岸田文雄宣称要“继承安倍遗志" }
      ]
    })
  }
  return (
    <div>
      <h2>组件内容</h2>
      <div>
        <ul className="nav nav-tabs">
          <li>
            <button onClick={toMusicHandle}>Message</button>
          </li>
          <li>
            <button onClick={toNewsHandle}>News</button>
          </li>
        </ul>
        <Outlet />
      </div>
    </div>
  )
}
