import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import store from './store/store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);

//store对象上有一个subscribe方法,当store内部的值发生改变的时候,subscribe方法就会执行
store.subscribe(() => {
  root.render(<App />);
})

