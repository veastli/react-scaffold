// 引入常量
import {
  INCREMEMTCOUNT,
  DECREMENTCOUNT
} from '../constant/countConst'
import store from '../store'
//累加的action
export const incrementAction = (data = 1) => {
  return {
    type: INCREMEMTCOUNT,
    data
  }
}
//累减的action
export const decrementAction = (data = 1) => {
  return {
    type: DECREMENTCOUNT,
    data
  }
}
// 异步编码的action
export const waitIncrementAction = (data) => {
  // 返回一个异步函数
  return () => {
    setTimeout(() => {
      store.dispatch({
        type: INCREMEMTCOUNT,
         data
      })
    }, 2000)
  }
}