import React, { useState } from 'react'
import axios from 'axios';
import PubSub from 'pubsub-js';

export default function Header() {
  const [search, setSearch] = useState("")

  const changeSearchHandle = (e) => {
    setSearch(e.target.value);
  }

  const searchHandle = async () => {
    // 发布
    PubSub.publish("github", { isInit: false, isSearch: true, data: [] })

    const result = await axios.get("https://api.github.com/search/users?q=" + search);
    PubSub.publish("github", { isInit: false, isSearch: false, data: result.data.items })
  }
  return (
    <section className="jumbotron">
      <h3 className="jumbotron-heading">Search Github Users</h3>
      <div>
        <input type="text" value={search} onChange={changeSearchHandle} placeholder="enter the name you search" />
        &nbsp;
        <button onClick={searchHandle}>Search</button>
      </div>
    </section>
  )
}
