import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { BrowserRouter } from 'react-router-dom'
import store from './store/store';
const root = ReactDOM.createRoot(document.getElementById('root'));
//把整个App中的组件放在一个路由器中,我们使用的是</BrowserRouter>
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

//store对象上有一个subscribe方法,当store内部的值发生改变的时候,subscribe方法就会执行
store.subscribe(() => {
  root.render(
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
})

