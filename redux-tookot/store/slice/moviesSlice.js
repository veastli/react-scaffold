import {
  createSlice,
  createAsyncThunk
} from "@reduxjs/toolkit";
// 引入axios请求
import {
  movieRequest
} from '../../api'
// 使用createAsyncThunk的方法来创建异步action
//createAsyncThunk接受两个参数,第一个参数是type的值,第二个参数是回调函数,回调函数就是异步action的函数
export const getMovieAction = createAsyncThunk('movie/getMovieList', async () => {
  const result = await movieRequest();
  console.log(result, 'result');
  return result.data.data.list
})

const movieSlice = createSlice({
  // 命名空间
  name: "movie",
  // 初始值
  initialState: {
    movieList: []
  },
  reducers: {

  },
  //异步action的处理,需要书写额外的reducer处理,不能写在reduers中,因为reducers会创建一个对应的action
  extraReducers: {
    [getMovieAction.fulfilled](prev, action) {
      console.log(prev,action,'prev');
      prev.movieList = action.payload
    },
    [getMovieAction.rejected]() {
      console.log("异步action失败--getMovieAction");
    },
    [getMovieAction.pending]() {
      console.log("异步action正在进行中--getMovieAction");
    },
  }
})
export default movieSlice.reducer;
