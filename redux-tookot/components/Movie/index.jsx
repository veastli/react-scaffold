import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getMovieAction } from "../../store/slice/moviesSlice"
export default function Movie() {
  // 返回Redux的store中对dispatch的引用
  const dispatch = useDispatch()
  const count = useSelector(state => state.count.count)
  const movieList = useSelector(state => state.movie.movieList)
  const getMovieHandle = () => {
    dispatch(getMovieAction())
  }
  return (
    <div>
      <h1>我是Movie组件</h1>
      <p>我收到的Count组件的值是 {count}</p>

      <button onClick={getMovieHandle}>获取电影的数据</button>
      <ul>
        {
          movieList.map(item => {
            return <li key={item.tvId}>{item.albumName}</li>
          })
        }
      </ul>
    </div>
  )
}
