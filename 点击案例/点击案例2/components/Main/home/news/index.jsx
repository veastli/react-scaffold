import React from 'react'
import { useLocation, useParams } from 'react-router-dom'
import qs from 'qs';
export default function News() {
  // 接受param传参
  const { id } = useParams()
    // 接受state传参
  const location = useLocation()
  // 接受search传递的参数
  const { user } = qs.parse(location.search.split("?")[1])
  
  const state = location.state
  return (<div>
    <p>用户名是{user},编号是:{id}</p>
    <ul>
      {
        state.map(item => {
          return <li key={item.id}>{item.name}</li>
        })
      }
    </ul>
  </div>
  )
}
