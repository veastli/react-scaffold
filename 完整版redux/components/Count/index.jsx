import React from 'react'
import store from "../../store/store"
import { incrementAction, decrementAction } from '../../store/actions/countAction';
export default function Count() {
  // 获取到reducer返回的值 getState()调用返回的值
  const { count } = store.getState()
  // 加
  const incrementHandle = () => {
    // 修改传过来的值
    store.dispatch(incrementAction(1))
  }

  const decreamentHandle = () => {
    store.dispatch(decrementAction(1))
  }
  // 奇数加一
  const ifOddDecrementHandle = () => {
    if (count % 2 == 1) {
      store.dispatch(incrementAction(1))
    }
  }
  // 3秒后执行加一
  const waitDecrementHandle = () => {
    setTimeout(() => {
      store.dispatch(incrementAction(1))
    }, 2000)
  }
  return (
    <div>
      <h1>我是Count组件 我的值是{count}</h1>
      <div>
        <button onClick={incrementHandle}>累加</button>
        <button onClick={decreamentHandle}>累减</button>
        <button onClick={ifOddDecrementHandle}>如果是奇数再加1</button>
        <button onClick={waitDecrementHandle}>等2s再加1</button>
      </div>

    </div>
  )
}
