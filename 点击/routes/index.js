// 路由表
// 引入需要集合的路由
import Count from '../pages/count'
import About from '../pages/about'
import List from '../components/list'
export default [
  {
    path: '/about',
    element: <About />
  },
  {
    path: '/count',
    element: <Count />,
    children: [
      {
        path: 'list',
        element: <List />,
      }]
  }
]