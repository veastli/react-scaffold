const init = {
  count: 0
}
export default function countReducer(prevStata = init, action) {
  // 接受action对象传过来的type和data
  const { type, data } = action
  console.log(prevStata.count,'count');
  // 根据类型进行判断该进行的行为
  switch (type) {
    // 如果是incrementHandle 则进行加法操作 同时也修改了的初始值 并将初始化值返回出去
    case 'incrementCount':
      return {
        ...init, count: prevStata.count + data
      };
    // 如果是decrementCount 则进行减法操作
    case 'decrementCount':
      return {
        ...init, count: prevStata.count - data
      };
  }
  // 返回初始值
  return prevStata
}