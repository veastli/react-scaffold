import React from 'react'

export default function Item({ avatar_url, login, url }) {
  return (
    <div className="card">
      <a href={url} target="_blank">
        <img src={avatar_url} style={{ width: "100px" }} />
      </a>
      <p className="card-text">{login}</p>
    </div >
  )
}