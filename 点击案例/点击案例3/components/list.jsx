import React from 'react'
// 引入组件 来接受state传参
import { useLocation, useParams } from 'react-router-dom'
import qs from 'qs';
export default function List() {
  // 接受params传参
  const { id } = useParams()
  console.log(id);
  // 接受state传参
  const location = useLocation()
  // 接受search传递的参数
  const { user } = qs.parse(location.search.split('?')[1])
  const state = location.state
  return (<div>
    <p>用户名是{id},编号是:{user}</p>
    <ul>
      {state.map(item => <li key={item.id}>{item.name}</li>)}
    </ul>
  </div>
  )
}
