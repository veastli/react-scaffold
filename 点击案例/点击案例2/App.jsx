import React from 'react'
// 引入路由链接  NavLink有高亮显示 而Link没有 关键字是active Navigate:重定向
//引入路由表 useRoutes路由表
import routes from './routes'
import { NavLink, useRoutes } from 'react-router-dom'

export default function App() {
  //解析路由表生成路由器和路由
  const element = useRoutes(routes)
  console.log(element,'element');
  return (
    <div>
      <div className="row">
        <div className="col-xs-offset-2 col-xs-8">
          <div className="page-header"><h2>React Router Demo</h2></div>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-2 col-xs-offset-2">
          <div className="list-group">
            {/* 路由链接  NavLink to*/}
            <NavLink className="list-group-item" to="/about">About</NavLink>
            {/* 加个end子路由触发时他也不会亮 */}
            <NavLink className="list-group-item" end to="/home">Home</NavLink>
          </div>
        </div>
        <div className="col-xs-6">
          <div className="panel">
            <div className="panel-body">
              {/* 注册路由 */}
              {element}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
