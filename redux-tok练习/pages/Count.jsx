import React from 'react'
// useDispatch:得到一个dispatch方法
//useSelector:参数是一个回调含糊,回调函数与的形参就是store中的数据
import { add, sub } from '../store/slice/countSlice'
import { useDispatch, useSelector } from "react-redux";
// 引入action函数
export default function Count() {
  const dispatch = useDispatch()
  const count = useSelector(state => state.count.count)
  const addition = () => {
    dispatch(add(1))
  }
  const subtraction = () => {
    dispatch(sub(1))
  }
  const OddNumber = () => {
    if (count % 2 === 1) {
      dispatch(add(1))
    }
  }
  const WaitforTwoseconds = () => {
    
  }
  return (
    <div>
      <h1>我是Count组件 我的值是{count}</h1>
      {/* <h2>当前的电影总是数 {movieList.length}</h2> */}
      <button onClick={addition}>累加</button>
      <button onClick={subtraction}>累减</button>
      <button onClick={OddNumber}>如果是奇数再加1</button>
      <button onClick={WaitforTwoseconds}>等2s再加1</button>
    </div>
  )
}
