// 引入常量
import {
  INCREMEMTCOUNT,
  DECREMENTCOUNT
} from '../constant/countConst'
//累加的action
export const incrementAction = (data = 1) => {
  return {
    type: INCREMEMTCOUNT,
    data
  }
}

//累减的action
export const decrementAction = (data = 1) => {
  return {
    type: DECREMENTCOUNT,
    data
  }
}