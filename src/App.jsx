// 包裹所有组件的包裹文'
import React from "react";
import Count from './pages/count'
import Movie from './pages/movie'
// 引入需要渲染的组件
export default function App() {
  return (
    <div>
      <Count />
      <hr />
      <Movie />
    </div>
  )
}
