import React, { useContext } from 'react'
import './index.css'
import Item from './Item'
import { AppContext } from '../../App'
export default function List() {
  const [todoList, setTodoList] = useContext(AppContext)
  console.log(todoList);
  return (
    <div>
      <ul className="todo-main">
        {/* 遍历  这里的item就是todoList里数组的每一项*/}
        {todoList.map(item => <Item key={item.id} {...item} todoList={todoList} setTodoList={setTodoList} />)}
      </ul>
    </div >
  )
}
