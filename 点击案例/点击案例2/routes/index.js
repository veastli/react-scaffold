import { lazy, Suspense } from 'react'
import { Navigate } from 'react-router-dom'
// 引入懒加载中加载文件
import Loading from '../pages/index'
// 路由器懒加载
const About = lazy(() => import('../components/Main/about'))
const Home = lazy(() => import('../components/Main/home'))
const News = lazy(() => import('../components/Main/home/news'))
const LazyLoad = (Comp) => {
  return (
    <Suspense fallback={<Loading />}>
      <Comp />
    </Suspense >
  )
}
// 暴露一个路由表 供外部引入
export default [
  {
    path: '/about',
    element: LazyLoad(About)
  },
  {
    path: '/home',
    element: LazyLoad(Home),
    // 配置子路由
    children: [
      {
        path: 'news/:id',
        element: LazyLoad(News)
      }
    ]
  },
  {
    path: '/',
    element: <Navigate to='/about' />
  }
]
//这样的写法,认为定义了 GetRoutes 组件 这个组件就是根据路由表生成的 路由配置结构
// export const GetRoutes = () => {
  //在组件中在外层可以使用hook
//   return useRoutes(routes)
// }

//自定义hooks
// export const useMyRoutes = () => {
  //在自定义hooks中是可以使用hook
//   return useRoutes(routes)
// }