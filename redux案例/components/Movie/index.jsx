import React from 'react'
import store from "../../store/store"
import { getMovieAction } from '../../store/actions/movieAction'
export default function Movie() {
  const { count } = store.getState().count
  const { movieList } = store.getState().movie;
  const getMovieHandle = () => {
    store.dispatch(getMovieAction())
  }
  return (
    <div>
      <h1>我是Movie组件</h1>
      <p>我收到的Count组件的值是:{count}</p>
      <button onClick={getMovieHandle}>获取</button>
      <ul>
        {movieList.map(item => {
          return <li key={item.tvId}>{item.albumName}</li>
        })}
      </ul>
    </div>
  )
}
