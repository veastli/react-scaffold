// 引入创建createSlice：接受一个初始状态和一个带有reducer名称和函数的查找表，并自动生成action creator函数、action类型字符串和一个reducer函数
import {
  createSlice
} from "@reduxjs/toolkit";


const countSlice = createSlice({
  // type的命名空间
  name: 'counter',
  // reducer提供的初始值
  initialState: {
    count: 0
  },
  reducers: {
    // 处理函数
    add(prev, action) {
      console.log(action, 'action');
      prev.count += action.payload
    },
    sub(prev, action) {
      console.log(action, 'action');
      prev.count -= action.payload
    },
  }
})
// 通过reducer属性,直接得到当前reducer函数 暴露出去供store使用
export default countSlice.reducer
// 通过actions属性将action暴漏出去
export const {
  add,
  sub
} = countSlice.actions