import React from 'react'
import "./index.css"
import Item from './Item/index'
// 接受父代传递的值 结构赋值直接拿值 接受方法 
export default function List({ todoList, setTodoList }) {
  return (
    <ul className="todo-main">
      {/* 遍历  这里的item就是todoList里数组的每一项*/}
      {todoList.map(item => <Item key={item.id} {...item} todoList={todoList} setTodoList={setTodoList} />)}
    </ul>
  )
}
