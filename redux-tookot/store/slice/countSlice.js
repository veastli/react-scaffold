import {
  createSlice
} from "@reduxjs/toolkit";

const countSlice = createSlice({
  //action中type的命名空间
  name: "counter",
  //reducer提供的初始值
  initialState: {
    count: 0
  },
  reducers: {
    increment(prev, action) {
      //reduce中处理函数接收两个参数 分别是当前reducer中上一次的值\本次dispacth的action对象
      //action对象中有一个payload就是用户调用action函数时传递的参数

      //不需要像之前那样得到新值再覆盖,可以直接修改reducer中的值,redux-toolkit中合成有一个immit的包帮我们处理了
      prev.count += action.payload
    },
    decrement(prev, action) {
      prev.count -= action.payload
    }
  }
})

//通过createSilce创建的对象上有一个reducer属性，直接得到就是当前的reducer函数，可以暴露出去给store使用
export default countSlice.reducer

export const {
  increment,
  decrement
} = countSlice.actions;
